#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = u'Example Author'
SITENAME = u"Example Notebook"
SITEURL = ''

TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
        ('Python.org', 'http://python.org/'))

DEFAULT_PAGINATION = 20

OUTPUT_RETENSION = (".hg", ".git", ".bzr")

THEME = "pelican-elegant"

PLUGIN_PATH = "pelican-plugins"
PLUGINS = ['sitemap', 'extract_toc', 'tipue_search', 'neighbors', 'render_math']
MD_EXTENSIONS = ['codehilite(css_class=highlight)', 'extra', 'headerid', 'toc']
DIRECT_TEMPLATES = (('index', 'tags', 'categories', 'archives',
                    'search', '404'))
STATIC_PATHS = ['theme/images', 'images', 'files', 'pelican-plugins']
TAG_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

# See https://github.com/getpelican/pelican/issues/1116
PATH = "content"
