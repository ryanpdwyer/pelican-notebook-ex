Example Lab Notebook
====================

This is an example lab notebook. Notebook contents are stored in reStructuredText files in the :code:`content` directory. For more information, see the file, :code:`content/pages/about-notebook.rst`.

Setting up the example notebook
-------------------------------

To clone the repository and download all submodules, install `git <http://git-scm.com>`_. Then using a git Bash or Terminal window, do

.. code:: bash

    git clone --recursive [https url at right of page]

To get all required python dependencies, use

.. code:: bash

    pip install pelican markdown Fabric BeautifulSoup4

Then you should be able to generate and view html by doing

.. code:: bash

    fab build
    fab serve

in the install directory. Use :code:`fab help` or :code:`make help` to see available options for building code and serving html.

Point a web browser at ``localhost:8000`` to see the output.

Starting your own notebook
--------------------------

To use this template to start your own lab notebook, start out by forking the repository.

.. image:: https://bytebucket.org/ryanpdwyer/pelican-notebook-ex/raw/bf02d6a3c98fb203c1ce3ad9b06a261a3fbe1956/content/images/fork.png
