A Post
######

:author: Ryan Dwyer
:date: 2014-05-13
:modified: 2014-05-13
:slug: a-post
:tags: pelican

This is an example post. Consider going to the `About Notebook <../pages/about-notebook.html>`_ page. Here is a link to the group photo.

.. image:: ../images/group-photo.jpg
	:alt: The Marohn group
