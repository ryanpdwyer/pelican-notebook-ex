About Notebook
##############

:author: Ryan Dwyer
:date: 2014-05-13
:modified: 2014-05-13
:slug: about-notebook

This is Ryan's description of his notebook structure; feel free to use this structure or modify as you see fit.

Notebook Structure
==================

This example project has a relevant file structure that looks like this::

    pelican-notebook-ex/
    └── content
        ├── a-post.rst
    	├── images
        │   └── group-photo.jpg
        └── pages
            └── about-notebook.rst


Useful Commands
===============

A link has the syntax ```link <url>`_``. See the `pelican documentation <http://docs.getpelican.com/en/3.3.0/getting_started.html#linking-to-internal-content>`_ for linking to other notebook pages. *Note: This may not be working on Windows.* Try the suggestion from `this github page <https://github.com/getpelican/pelican/issues/1305>`_.

To link to a static path, such as an image, add the folder containing the files to :code:`STATIC_PATHS` in `pelicanconf.py`.


Useful git commands
===================

To get a nice command line view of changes (like mercurial shows by default), use :code:`git diff`.

Theme
=====

The theme is Elegant-1.3, modified by Ryan to have more sane bulleted lists. See `ryanpdwyer/elegant <https://github.com/ryanpdwyer/pelican-elegant/tree/rpd-edit>`_.

MathJax
=======

My current implementation of MathJax uses the Pelican plugin latex_. To include math, use ``$`` and double backslashes ``\\``. For example, 

.. code::
	
	$\\Gamma = \\frac{f_c}{\\omega_c Q}$

will be marked up as $\\Gamma = \\frac{f_c}{\\omega_c Q}$.

.. _latex: https://github.com/barrysteyn/pelican_plugin-latex


